const sets = require('./settings');
var mysql = require('mysql');
const pool = mysql.createPool(sets.database);


const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(sets.bot_token, {
    webHook: {
        port: sets.port,
        autoOpen:false
    }
});

bot.openWebHook()
bot.setWebHook(`${sets.url}/bot${sets.bot_token}`)

bot.onText(/\/start/, (msg) =>{
    const chatId = msg.chat.id;
    const text = msg.text;
    const name = msg.chat.first_name
    const username = msg.chat.username
    var days = [
        'Воскресенье',
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
      ];
      var d = new Date();
      var n = d.getDay();


    pool.getConnection(function(err, conn){
        if(err) throw err
        conn.query(`SELECT chat_id FROM users WHERE chat_id=${chatId}`, function(err, res, fields){ 
            if(err) throw err;
            if(res && res.length > 0){
                return false
            }
            conn.query(`INSERT INTO users(chat_id,name,username) VALUES(${chatId},'${name}','${username}')`, function(err){
                if(err) throw err
            })
            return true
        })
    conn.release()
    })
    pool.getConnection(function(err,conn){
        if(err) throw err
        switch(days[n]) {
            case 'Понедельник': 
            conn.query('SELECT * FROM monday', function(err,res){
                if(err) throw err
                let temp = ''
                res.forEach(element => {
                    temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                });
                bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{parse_mode:'HTML'})
            })
            break;
            case 'Вторник':
                conn.query('SELECT * FROM tuesday', function(err,res){
                    if(err) throw err
                    let temp = ''
                    res.forEach(element => {
                        temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                    });
                    bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{parse_mode:'HTML'})
                })
                break;
                case 'Среда':
                conn.query('SELECT * FROM wednesday', function(err,res){
                    if(err) throw err
                    let temp = ''
                    res.forEach(element => {
                        temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                    });
                    bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{parse_mode:'HTML'})
                })
                break;
                case 'Четверг':
                conn.query('SELECT * FROM thursday', function(err,res){
                    if(err) throw err
                    let temp = ''
                    res.forEach(element => {
                        temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                    });
                    bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{reply_markup:{keyboard:['Тык сюда🧐'],resize_keyboard:true}},{parse_mode:'HTML'})
                })
                break;
                case 'Пятница':
                conn.query('SELECT * FROM friday', function(err,res){
                    if(err) throw err
                    let temp = ''
                    res.forEach(element => {
                        temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                    });
                    bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{parse_mode:'HTML'})
                })
                break;
                case 'Суббота':
                conn.query('SELECT * FROM saturday', function(err,res){
                    if(err) throw err
                    let temp = ''
                    res.forEach(element => {
                        temp+= `${element.id}   ${element.subjects}  ${element.time}\n`
                    });
                    bot.sendMessage(chatId, `Сегодня: ${days[n]} 
${temp}`,{parse_mode:'HTML'})
                })
                break;
                default: bot.sendMessage(chatId, 'Сегодня нет уроков не парься')
        }
        conn.release()
    })
    
});
